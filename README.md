# WhereAreMyGlasses
Simplistic and modifiable watchface for Garmin ConnectIQ watches.

![Sample on Garmin vivoactive 3](/etc/vivoactive_3.png)

Show's the time in the largest font available plus a short date string. Use either Garmin Express or Garmin Connect to configure the watchface. The following options are provided:
 - Color of each background, hours, minutes, date and even the separating line :)
 - Setting the color of any item to the color of the background will make it go away
 - Threshold value to display the battery indicator (default: 30%)

Look ![here](https://developer.garmin.com/connect-iq/user-experience-guide/page-layout/index.htm#Bitmaps) for all available color codes.

It's open source! Feel free to have a look at the code, propose new features or report issues here: ![WhereAreMyGlasses](https://github.com/knusprjg/WhereAreMyGlasses).

Enjoy!
